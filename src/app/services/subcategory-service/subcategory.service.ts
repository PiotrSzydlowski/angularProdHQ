import { Injectable } from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {AddSubcategory} from '../../model/AddSubcategory';
import {Observable} from 'rxjs';
import {Subcategory} from '../../model/Subcategory';
import {tap} from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class SubcategoryService {

  private url = 'http://localhost:9193/prod/api/subcategories';

  constructor(private http: HttpClient) {
  }

  saveSubCategory(saveSubCategory: Partial<AddSubcategory>): Observable<HttpResponse<Subcategory>> {
    return this.http.post(this.url + '/addSubcategory', saveSubCategory, {observe: 'response'})
      .pipe(tap(console.log));
  }

  getSubcategory() {
    return this.http.get(this.url + '/subcategory')
      .pipe(tap(console.log));
  }

  getSubcategoryByParentId(id: string) {
    return this.http.get(this.url + '/subcategory/parent/' + id)
      .pipe(tap(console.log));
  }

}
