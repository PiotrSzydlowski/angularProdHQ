import {Injectable} from "@angular/core";

@Injectable({
  providedIn: 'root'
})

export class DataCategoryService {

  id: string;

  setCategoryIdData(id: string): void {
    this.id = id;
  }

  getCategoryIdData(): string | null {
    return this.id;
  }
}
