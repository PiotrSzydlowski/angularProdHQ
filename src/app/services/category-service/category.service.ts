import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';

import {multicast, refCount, tap} from 'rxjs/operators';
import {Observable, Subject} from 'rxjs';
import {CategoryTree} from '../../model/CategoryTree';
import {AddCategory} from '../../model/AddCategory';
import {Category} from '../../model/Category';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  private url = 'http://localhost:9193/prod/api/categories';

  constructor(private http: HttpClient) {
  }

  getCategoryTree() {
    return this.http.get<CategoryTree[]>(this.url + '/categoryTree')
      .pipe(multicast(new Subject<CategoryTree[]>()), refCount());
  }

  switchActive(id: string) {
    return this.http.get(this.url + '/switchCategoryActive/' + id)
      .pipe(tap(console.log));
  }

  deleteCategory(id: string) {
    return this.http.delete(this.url + '/deleteCategory/' + id)
      .pipe(tap(console.log));
  }

  saveCategory(saveCategory: Partial<AddCategory>): Observable<HttpResponse<Category>> {
    return this.http.post(this.url + '/addCategory', saveCategory, {observe: 'response'})
      .pipe(tap(console.log));
  }

}
