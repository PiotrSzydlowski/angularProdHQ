import { Injectable } from '@angular/core';
import {tap} from "rxjs/operators";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class StockService {

  mag: number;
  productId: number;
  new: boolean;
  cold: boolean;
  hit: boolean;

  constructor(private http: HttpClient) {
  }

  private url = 'http://localhost:9193/prod/api/stocks';

  getStockListByMagId(id: string) {
    return this.http.get(this.url + '/stock/' + id)
      .pipe(tap(console.log));
  }

  setDataToAddQty(mag, id: number) {
    this.mag = mag;
    this.productId = id;
  }

  setProductId(id: number) {
    this.productId = id;
  }

  setMag(mag: number) {
    this.mag = mag;
  }

  public geMagData(): number {
    return this.mag;
  }

  public getProductId(): number {
    return this.productId;
  }

  addQtyToStock(mag, prod, qty: number) {
    return this.http.post(this.url + '/addQty/' + mag + '/' + prod + '/' + qty, null)
      .pipe(tap());
  }

  minusQtyToStock(mag, prod, qty: number) {
    return this.http.post(this.url + '/minusQty/' + mag + '/' + prod + '/' + qty, null)
      .pipe(tap());
  }

  changeProductPrice(price, mag, id: number) {
    return this.http.post(this.url + '/changePrice/' + price + '/' + mag + '/' + id, null)
      .pipe(tap());
  }

  setPromo(mag, prod, price) {
    return this.http.post(this.url + '/setPromo/' + mag + '/' + prod + '/' + price, null)
      .pipe(tap());
  }

  setArtibiute(cold: boolean, hit: boolean, _new: boolean) {
    this.cold = cold;
    this.hit = hit;
    this.new = _new;
  }

  getNewAtribute(): boolean {
    return this.new;
  }

  getColdArtibiute(): boolean {
    return this.cold;
  }

  getHitAtribute(): boolean {
    return this.hit;
  }

  setAttributes(cold, hit, _new, mag, prod) {
    return this.http.post(this.url + '/setAttribute/' + cold + '/' + hit + '/' + _new + '/' + mag + '/' + prod, null)
      .pipe(tap());
  }

}
