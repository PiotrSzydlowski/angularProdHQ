import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {AddProduct} from '../../model/AddProduct ';
import {Product} from '../../model/Product';
import {ContractorSave} from '../../model/ContractorSave';
import {EditProduct} from '../../model/EditProduct';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private url = 'http://localhost:9193/prod/api/products';
  formData = new FormData();
  product: Product;

  constructor(private http: HttpClient) {
  }

  getProductsList() {
    return this.http.get(this.url + '/products')
      .pipe(tap(console.log));
  }

  changeProductStatus(id: string): Observable<HttpResponse<any>> {
    return this.http.post(this.url + '/changeProductActive/' + id, {observe: 'response'})
      .pipe(tap(console.log));
  }

  addProduct(model: Partial<AddProduct>): Observable<HttpResponse<any>> {
    return this.http.post(this.url + '/addproduct', model, {observe: 'response'})
      .pipe();
  }

  addPhoto(File, id: string) {
    this.formData.append('file', File);
    return this.http.post(this.url + '/uploadFile/' + id, this.formData)
      .pipe();
    this.clearFormData();
  }

  clearFormData() {
    this.formData = null;
  }

  deletePhoto(id: string) {
    return this.http.delete(this.url + '/deletePhoto/' + id).pipe();
  }

  deleteProduct(id: string): Observable<HttpResponse<any>> {
    return this.http.delete(this.url + '/deleteProduct/' + id, {observe: 'response'})
      .pipe(tap(console.log));
  }

  editProduct(updateProduct: Partial<EditProduct>): Observable<HttpResponse<any>> {
    return this.http.put(this.url + '/updateProduct', updateProduct, {observe: 'response'})
      .pipe(tap(console.log));
  }

  setProduct(product: Product) {
    this.product = product;
  }

  getProduct(): Product {
    return this.product;
  }
}
