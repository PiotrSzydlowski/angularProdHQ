import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UnitService {

  private url = 'http://localhost:9193/prod/api/units';

  constructor(private http: HttpClient) {
  }

  getUnits() {
    return this.http.get(this.url + '/units')
      .pipe(tap(console.log));
  }
}
