import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class StoreService {

  storeId: string;

  constructor(private http: HttpClient) {
  }

  private url = 'http://localhost:9193/prod/api/stores';

  getStoresList() {
    return this.http.get(this.url + '/stores')
      .pipe(tap(console.log));
  }

  changeStoreActive(id: string) {
    return this.http.post(this.url + '/changeStoreActive/' + id, null)
      .pipe(tap());
  }

  changeTempAvaStore(id: string) {
    return this.http.post(this.url + '/changeTempAvaStore/' + id, null)
      .pipe(tap());
  }

  changeOpenHour(from: string, to: string, id: string) {
    return this.http.post(this.url + '/changeOpenHour/' + from + '/' + to + '/' + id, null)
      .pipe(tap());
  }

  setStoreId(id: string) {
    this.storeId = id;
  }

  getStoreId(): string {
    return this.storeId;
  }
}
