import {Injectable} from '@angular/core';
import {ContractorSave} from "../../model/ContractorSave";
import {ContractorGet} from "../../model/ContractorGet";

@Injectable({
  providedIn: 'root'
})
export class DataUserServiceService {

  userLogin: string;
  contractor: ContractorSave;

  setData(login: string): void {
    this.userLogin = login;
  }

  getData(): string | null {
    return this.userLogin;
  }

  setContractor(contractor: ContractorGet): void {
    this.contractor = contractor;
  }

  getContractor(): ContractorSave {
    return this.contractor;
  }
}
