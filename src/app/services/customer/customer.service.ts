import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {tap} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  private url = 'http://localhost:9193/prod/api/customers';

  constructor(private http: HttpClient) { }

  getCustomers() {
    return this.http.get(this.url + '/customers')
      .pipe(tap(console.log));
  }
}
