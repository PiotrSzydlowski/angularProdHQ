import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import {multicast, refCount, tap} from "rxjs/operators";
import {Observable, Subject} from "rxjs";
import {ContractorGet} from "../../model/ContractorGet";
import {ContractorSave} from "../../model/ContractorSave";

@Injectable({
  providedIn: 'root'
})
export class ContractorServiceService {

  private url = 'http://localhost:9193/prod/api/contractors';
  formData = new FormData();

  constructor(private http: HttpClient) { }

  getContractors() {
    return this.http.get<ContractorGet[]>(this.url + '/contractors')
      .pipe(multicast(new Subject<ContractorGet[]>()), refCount());
  }

  getSuppliers() {
    return this.http.get<ContractorGet[]>(this.url + '/contractors/suppliers')
      .pipe(multicast(new Subject<ContractorGet[]>()), refCount());
  }

  getCustomers() {
    return this.http.get<ContractorGet[]>(this.url + '/contractors/customers')
      .pipe(multicast(new Subject<ContractorGet[]>()), refCount());
  }

  deleteContractor(id: string): Observable<HttpResponse<any>> {
    return this.http.delete(this.url + '/deleteContractor/' + id, {observe: 'response'})
      .pipe(tap(console.log));
  }

  saveContractor(saveContractor: Partial<ContractorSave>): Observable<HttpResponse<any>> {
    return this.http.post(this.url + '/addContractor', saveContractor, {observe: 'response'})
      .pipe(tap(console.log));
  }

  updateContractor(updateContractor: Partial<ContractorSave>): Observable<HttpResponse<any>> {
    return this.http.put(this.url + '/updateContractor', updateContractor, {observe: 'response'})
      .pipe(tap(console.log));
  }

  uploadFile(file): Observable<HttpResponse<any>> {
    this.formData.append('file', file);
    return this.http.post('http://localhost:9193/prod/api/uploadContractors', this.formData, {observe: 'response'})
      .pipe(tap(console.log));
  }
}
