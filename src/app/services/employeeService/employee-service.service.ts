import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {EmployeesGet} from '../../model/employeesGet';
import {multicast, refCount, tap} from 'rxjs/operators';
import {Observable, Subject} from 'rxjs';
import {LoginUserPost} from '../../model/LoginUserPost';
import {LoginUserGet} from '../../model/LoginUserGet';
import {EmployeeSave} from '../../model/employee-save';

@Injectable({
  providedIn: 'root'
})
export class EmployeeServiceService {

  private url = 'http://localhost:9193/prod/api/users';

  constructor(private http: HttpClient) {
  }

  getEmployees() {
    return this.http.get<EmployeesGet[]>(this.url + '/user/employee')
      .pipe(multicast(new Subject<EmployeesGet[]>()), refCount());
  }

  saveEmployee(saveEmployee: Partial<EmployeeSave>): Observable<HttpResponse<any>> {
    return this.http.post(this.url + '/addUser', saveEmployee, {observe: 'response'})
      .pipe(tap(console.log));
  }

  deleteEmployee(id: number): Observable<HttpResponse<any>> {
    return this.http.delete(this.url + '/deleteUser/' + id, {observe: 'response'})
      .pipe(tap(console.log));
  }

  changeActive(id: string): Observable<HttpResponse<any>> {
    return this.http.get(this.url + '/changeActive/' + id, {observe: 'response'})
      .pipe(tap(console.log));
  }
}
