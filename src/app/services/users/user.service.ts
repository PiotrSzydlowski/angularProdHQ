import { Injectable } from '@angular/core';
import {HttpClient, HttpResponse} from "@angular/common/http";
import {Observable} from "rxjs";
import {tap} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private url = 'http://localhost:9193/prod/api/users';

  constructor(private http: HttpClient) { }

  changeUserBanned(id: string): Observable<HttpResponse<any>> {
    return this.http.get(this.url + '/changeBanned/' + id, {observe: 'response'})
      .pipe(tap(console.log));
  }
}
