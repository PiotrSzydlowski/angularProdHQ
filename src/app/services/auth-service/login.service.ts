import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map, tap} from 'rxjs/operators';
import {LoginUserGet} from '../../model/LoginUserGet';
import {LoginUserPost} from '../../model/LoginUserPost';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private url = 'http://localhost:9193/prod/user';
  constructor(private http: HttpClient) {
  }

  options = {
    headers: new HttpHeaders({ 'login-type': 'login' }),
    observe: "response" as 'body'
  };

  logIn(loginUserPost: Partial<LoginUserPost>): Observable<HttpResponse<LoginUserGet>> {
   return this.http.post(this.url + '/login', loginUserPost, this.options)
      .pipe(tap(console.log));
  }
}
