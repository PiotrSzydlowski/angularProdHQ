export interface EditProduct {
  id: string;
  ean: string;
  unit: string;
  category: string;
  name: string;
  description: string;
  subcategory: string;
}
