export interface StockItem {
  id: string;
  store: string;
  productsName: string;
  productId: number;
  quantity: number;
  price: number;
  hit: boolean;
  promo: boolean;
  cold: boolean;
  active: boolean;
  reservedQuantity: number;
  priceBeforePromo: number;
  _new: boolean;
}
