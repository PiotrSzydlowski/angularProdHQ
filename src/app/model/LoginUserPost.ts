export interface LoginUserPost {
  login: string;
  password: string;
}
