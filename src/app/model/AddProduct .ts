export interface AddProduct {
  ean: string;
  unit: string;
  is_active: boolean;
  category: string;
  name: string;
  description: string;
  subcategory: string;
}
