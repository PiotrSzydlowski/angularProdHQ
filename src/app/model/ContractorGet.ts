export interface ContractorGet {
  id: string;
  name: string;
  address: string;
  city: string;
  postalCode: string;
  phone: string;
  nip: string;
  email: string;
  contractorTypeId: string;
  contractorType: string;
}
