export interface ContractorSave {
  id?: string;
  name: string;
  address: string;
  city: string;
  postalCode: string;
  phone: string;
  nip: string;
  email: string;
  contractorType: string;
}
