export interface Category {
  id: string;
  name: string;
  parent_id: string | null;
  _active: boolean;
}
