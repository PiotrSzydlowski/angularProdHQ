export interface Customers {
  id: string;
  name: string;
  active: boolean;
  email: string;
  _vip: boolean;
  _fraud: boolean;
  _banned: boolean;
}
