export interface Product {
  id: string;
  ean: string ;
  name: string | null;
  description: string | null;
  categoryId: string | null;
  categoryName: string | null;
  categoryActive: boolean;
  subcategoryId: string | null;
  subcategoryName: string | null;
  subcategoryParentId: string | null;
  unitName: string | null;
  photo: string ;
  active: boolean;
  grammage: string | null;
}
