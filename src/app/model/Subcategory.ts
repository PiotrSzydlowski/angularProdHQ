export interface Subcategory{
  id: string;
  name: string;
  _active: boolean;
}
