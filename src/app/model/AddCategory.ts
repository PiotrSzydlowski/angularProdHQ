export interface AddCategory {
  name: string;
  is_active: boolean;
}
