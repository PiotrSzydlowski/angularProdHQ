export interface LoginUserGet {
  id: string;
  login: string;
  role: string;
}
