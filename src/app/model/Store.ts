import {Addresses} from './Addresses';


export interface Store {
  id: string;
  active: boolean;
  defoult: boolean;
  open_from: string;
  open_to: string;
  temporaryavaiable: boolean;
  addresses: {
    [key: string]: Addresses
  };
}
