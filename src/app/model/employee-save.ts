export interface EmployeeSave {
  login: string;
  password: string;
  role: string;
}
