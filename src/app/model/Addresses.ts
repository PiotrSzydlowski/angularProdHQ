export interface Addresses {
  id: string;
  street: string;
  street_number: string;
  door_number: string | null;
  flor: string | null;
  latidute: string | null;
  longtitiude: string | null;
  postal_code: string;
  message: string | null;
}
