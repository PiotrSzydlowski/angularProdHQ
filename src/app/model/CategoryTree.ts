import {Subcategory} from './Subcategory';

export interface CategoryTree {
  id: string;
  name: string;
  _active: boolean;
  subcategory: {
    [key: string]: Subcategory
  };
}
