export interface AddSubcategory {
  name: string;
  is_active: boolean;
  parent_id: string;
}

