export interface EmployeesGet {
  id: string;
  login: string;
  role: string;
  active: boolean;
}
