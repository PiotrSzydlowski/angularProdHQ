import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AdminDashboardComponent} from "./components/admin-dashboard/admin-dashboard.component";
import {HomeComponent} from "./components/home/home.component";
import {
  AddContractorDialogComponent
} from "./components/contractors/contractors/add-contracor-dialog/add-contractor-dialog/add-contractor-dialog.component";
import {AddCategoryComponent} from "./components/category/modal/add-category/add-category.component";
import {EmployeeComponent} from "./components/employee/employee/employee.component";
import {EditHourMagazineComponent} from "./components/store/store/edit-hour-magazine/edit-hour-magazine.component";
import {
  ConfirmDeleteModalComponent
} from "./components/category/modal/confirm-delete-modal/confirm-delete-modal.component";
import {ClientComponent} from "./components/client/client/client.component";
import {SetAtribiuteComponent} from "./components/stock/stock/set-atribiute/set-atribiute.component";
import {MatMenuModule} from "@angular/material/menu";
import {MatFormFieldModule} from "@angular/material/form-field";
import {SearchFilterPipe} from "./components/contractors/contractors/search-filter.pipe";
import {CategoryComponent} from "./components/category/category.component";
import {
  EditProductDialogComponentComponent
} from "./components/product/product/editProductDialog/edit-product-dialog-component/edit-product-dialog-component.component";
import {StoreComponent} from "./components/store/store/store.component";
import {ContractorsComponent} from "./components/contractors/contractors/contractors.component";
import {AddDialogComponent} from "./components/employee/employee/dialogs/add-dialog/add-dialog.component";
import {HeaderComponent} from "./components/header/header.component";
import {FooterComponent} from "./components/footer/footer.component";
import {
  EditContractorDialogComponent
} from "./components/contractors/contractors/edit-contractor-dialog/edit-contractor-dialog/edit-contractor-dialog.component";
import {
  AddSubcategoryModalComponent
} from "./components/category/modal/add-subcategory-modal/add-subcategory-modal.component";
import {ProductComponent} from "./components/product/product/product.component";
import {StockComponent} from "./components/stock/stock/stock.component";
import {SetpromoComponent} from "./components/stock/stock/setpromo/setpromo.component";
import {AddQtyComponent} from "./components/stock/stock/add-qty/add-qty.component";
import {MinusQtyComponent} from "./components/stock/stock/minus-qty/minus-qty.component";
import {ChangePriceComponent} from "./components/stock/stock/change-price/change-price.component";
import {AdminRoutingModule} from "./admin-routing.module";
import {MatButtonModule} from "@angular/material/button";
import {MatBadgeModule} from "@angular/material/badge";
import {MatDialogModule} from "@angular/material/dialog";
import {MatInputModule} from "@angular/material/input";
import {MatSelectModule} from "@angular/material/select";
import {MatOptionModule} from "@angular/material/core";
import {MatTableModule} from "@angular/material/table";
import {MatPaginatorModule} from "@angular/material/paginator";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatRadioModule} from "@angular/material/radio";
import {MatButtonToggleModule} from "@angular/material/button-toggle";
import {MatIconModule} from "@angular/material/icon";
import {MatChipsModule} from "@angular/material/chips";




@NgModule({
  declarations: [AdminDashboardComponent, HeaderComponent, FooterComponent, HomeComponent,
     EmployeeComponent, AddDialogComponent, ContractorsComponent, AddContractorDialogComponent,
    EditContractorDialogComponent, SearchFilterPipe, CategoryComponent, AddCategoryComponent,
    ConfirmDeleteModalComponent, AddSubcategoryModalComponent, ProductComponent, EditProductDialogComponentComponent, StockComponent,
    ClientComponent, StoreComponent, EditHourMagazineComponent, SetpromoComponent, AddQtyComponent, MinusQtyComponent,
    ChangePriceComponent, SetAtribiuteComponent],
    imports: [
        CommonModule,
        AdminRoutingModule,
        MatButtonModule,
        MatBadgeModule,
        MatMenuModule,
        MatDialogModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatOptionModule,
        MatTableModule,
        MatPaginatorModule,
        FormsModule,
        MatRadioModule,
        MatButtonToggleModule,
        MatIconModule,
        MatChipsModule,
        ReactiveFormsModule
    ]
})
export class AdminModule { }
