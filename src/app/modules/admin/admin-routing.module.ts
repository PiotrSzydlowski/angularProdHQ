import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AdminDashboardComponent} from './components/admin-dashboard/admin-dashboard.component';
import {HomeComponent} from './components/home/home.component';
import {EmployeeComponent} from './components/employee/employee/employee.component';
import {ContractorsComponent} from './components/contractors/contractors/contractors.component';
import {CategoryComponent} from './components/category/category.component';
import {ProductComponent} from './components/product/product/product.component';
import {StockComponent} from "./components/stock/stock/stock.component";
import {ClientComponent} from "./components/client/client/client.component";
import {StoreComponent} from "./components/store/store/store.component";


const routes: Routes = [
  {path: '', component: AdminDashboardComponent, children: [
      {path: 'home', component: HomeComponent},
      {path: 'employee', component: EmployeeComponent},
      {path: 'contractors', component: ContractorsComponent},
      {path: 'category', component: CategoryComponent},
      {path: 'product', component: ProductComponent},
      {path: 'stock', component: StockComponent},
      {path: 'clients', component: ClientComponent},
      {path: 'store', component: StoreComponent},
      {path: '', redirectTo: '/admin/home', pathMatch: 'full'}
    ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
