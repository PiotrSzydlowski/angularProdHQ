import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../../../services/auth-service/auth.service';
import {DataUserServiceService} from '../../../../services/data-user-service/data-user-service.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  user: string;

  constructor(private auth: AuthService, private data: DataUserServiceService) {
  }

  ngOnInit(): void {
    this.data.getData();
    this.user = this.data.getData();
    // if (this.user === undefined) {
    //   this.logout();
    // }
  }

  logout(): void {
    this.auth.logout();
  }
}
