import {Component, OnInit} from '@angular/core';
import {DataCategoryService} from "../../../../../../services/category-service/dataCategoryService";
import {CategoryService} from "../../../../../../services/category-service/category.service";


@Component({
  selector: 'app-confirm-delete-modal',
  templateUrl: './confirm-delete-modal.component.html',
  styleUrls: ['./confirm-delete-modal.component.css']
})
export class ConfirmDeleteModalComponent implements OnInit {

  id: string;

  constructor(public data: DataCategoryService, public http: CategoryService) {
  }

  ngOnInit(): void {
  }

  closeDialog() {
    console.log('DDDDDD');
  }

  deleteCategory() {
    this.id = this.data.getCategoryIdData();
    this.http.deleteCategory(this.id).subscribe();
  }
}
