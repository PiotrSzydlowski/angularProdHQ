import {Component, OnInit, ViewChild} from '@angular/core';
import {CategoryService} from '../../../../services/category-service/category.service';
import {CategoryTree} from '../../../../model/CategoryTree';
import {MatDialog} from '@angular/material/dialog';
import {DataCategoryService} from '../../../../services/category-service/dataCategoryService';
import {ConfirmDeleteModalComponent} from './modal/confirm-delete-modal/confirm-delete-modal.component';
import {AddCategory} from '../../../../model/AddCategory';
import {NgForm} from '@angular/forms';
import {SubcategoryService} from '../../../../services/subcategory-service/subcategory.service';
import {AddSubcategory} from '../../../../model/AddSubcategory';




@Component({
  selector: 'app-products',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  constructor(http: CategoryService, public dialog: MatDialog, public categoryData: DataCategoryService,
              private categoryHttp: CategoryService, private subcategoryHttp: SubcategoryService) {
    this.http = http;
  }

  @ViewChild('formCat') formCat!: NgForm;
  @ViewChild('subForm') subForm!: NgForm;
  categoryModel: Partial<AddCategory> = {};
  subcategoryModel: Partial<AddSubcategory> = {};
  category: any = {};
  private http: CategoryService;
  categoryTree: CategoryTree[];

  ngOnInit(): void {
    this.refresh();
    const categories$ = this.http.getCategoryTree();
    categories$.subscribe(cat => {
      this.category = cat;
    });

  }

  refresh() {
    const categoryTree$ = this.http.getCategoryTree();
    categoryTree$.subscribe(tree => {
      this.categoryTree = tree;
      console.log(this.categoryTree);
    }, error => {
      console.log(error.errors);
    });
  }

  switchActive(id: string) {
    this.http.switchActive(id).subscribe(res => {
      console.log('zmieniono active dla categorii');
      this.refresh();
    }, error => {
      console.log('Coś poszło nie tak');
    });
  }

  deleteCategoryDialog(id: string) {
    this.categoryData.setCategoryIdData(id);
    this.dialog.open(ConfirmDeleteModalComponent, {
      height: '35%',
      width: '30%'
    });
  }

  deleteSubcategory(id: string) {
    this.http.deleteCategory(id).subscribe();
    this.refresh();

  }

  addCategory(formCat: NgForm) {
    this.categoryHttp.saveCategory(this.categoryModel).subscribe({
        next: () => {
          this.refresh();
        },
        error: () => {
          console.log('porażka');
        }
      }
    );
    this.formCat.resetForm();
    console.log(this.categoryModel.name + ' ' + this.categoryModel.is_active);
  }

  addSubCategory(subForm: NgForm) {
    this.subcategoryHttp.saveSubCategory(this.subcategoryModel).subscribe({
      next: () => {
        this.refresh();
      },
      error: () => {
        console.log('nie pykło');
      }
    });
    this.subForm.resetForm();
  }

}
