import {Component, OnInit} from '@angular/core';
import {Store} from '../../../../../model/Store';
import {StoreService} from '../../../../../services/store-service/store.service';
import {MatDialog} from "@angular/material/dialog";
import {EditHourMagazineComponent} from "./edit-hour-magazine/edit-hour-magazine.component";

@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.css']
})
export class StoreComponent implements OnInit {

  store: Store[];

  constructor(private httpStore: StoreService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.getStore();
  }

  getStore() {
    this.httpStore.getStoresList().subscribe({
      next: store => {
        console.log('Magazyny pobrany');
        this.store = store;
        console.log('Magazyny z metody ' + this.store);
      },
      error: () => {
        console.log('Magazyny nie pobrane');
      }
    });
  }

  activeStore(id: string) {
    this.httpStore.changeStoreActive(id)
      .subscribe({
        next: () => {
          this.getStore();
        }
      });
  }

  changeHours(id: string) {
    this.httpStore.setStoreId(id);
    this.dialog.open(EditHourMagazineComponent, {
      height: '20%',
      width: '20%'
    });
  }

  changeTempAvaiable(id: string) {
    this.httpStore.changeTempAvaStore(id)
      .subscribe({
        next: () => {
          this.getStore();
        }
      });
  }

  catchement() {
    this.getStore();
  }
}
