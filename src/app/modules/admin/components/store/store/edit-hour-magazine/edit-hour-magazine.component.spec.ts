import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditHourMagazineComponent } from './edit-hour-magazine.component';

describe('EditHourMagazineComponent', () => {
  let component: EditHourMagazineComponent;
  let fixture: ComponentFixture<EditHourMagazineComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditHourMagazineComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditHourMagazineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
