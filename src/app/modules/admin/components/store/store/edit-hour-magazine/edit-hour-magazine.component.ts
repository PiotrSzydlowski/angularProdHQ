import {Component} from '@angular/core';
import {StoreService} from '../../../../../../services/store-service/store.service';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-edit-hour-magazine',
  templateUrl: './edit-hour-magazine.component.html',
  styleUrls: ['./edit-hour-magazine.component.css']
})
export class EditHourMagazineComponent {

  openFrom: string;
  openTo: string;

  constructor(private httpStore: StoreService) {
  }

  changeHourFrom(value) {
    this.openFrom = value;
  }

  changeHourTo(value) {
    this.openTo = value;
  }

  EditHour() {
    if (this.openFrom == null || this.openTo == null) {
      console.log('wartości = null');
    } else {
      this.httpStore.changeOpenHour(this.openFrom, this.openTo, this.httpStore.getStoreId())
        .subscribe({
          next: () => {
          }
        });
    }
  }
}
