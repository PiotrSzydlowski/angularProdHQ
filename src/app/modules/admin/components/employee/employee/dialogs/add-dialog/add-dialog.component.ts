import {Component} from '@angular/core';
import {NgForm} from '@angular/forms';
import {EmployeeSave} from '../../../../../../../model/employee-save';
import {Router} from '@angular/router';
import {EmployeeServiceService} from '../../../../../../../services/employeeService/employee-service.service';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-add-dialog',
  templateUrl: './add-dialog.component.html',
  styleUrls: ['./add-dialog.component.css']
})
export class AddDialogComponent {

  model: Partial<EmployeeSave> = {};

  constructor(private router: Router, private http: EmployeeServiceService, private snackBar: MatSnackBar) {
  }

  saveEmployee(form: NgForm) {
    this.http.saveEmployee(this.model).subscribe(res => {
        console.log('save employee from ' + res);
        this.openSnackBar('Pracownik poprawnie dodany');
        this.http.getEmployees();
      }, error => {
        console.log('save employee from error  ' + error.error);
        this.openSnackBar('Problem z dodaniem pracownika - pracownik o takim loginie istnieje');
      }
    );
  }

  openSnackBar(message: string) {
    this.snackBar.open(message, 'Zamknij', {
      duration: 3000
    });
  }
}
