import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {EmployeeServiceService} from '../../../../../services/employeeService/employee-service.service';
import {EmployeesGet} from '../../../../../model/employeesGet';
import {MatDialog} from '@angular/material/dialog';
import {AddDialogComponent} from './dialogs/add-dialog/add-dialog.component';
import {Subscription, timer} from 'rxjs';
import {map} from 'rxjs/operators';
import {MatSnackBar} from '@angular/material/snack-bar';


@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  private http: EmployeeServiceService;
  employees: EmployeesGet[];
  displayedColumns: string[] = ['id', 'login', 'role', 'active', 'action'];
  selectedRow;
  timerSubscription: Subscription;

  constructor(http: EmployeeServiceService, public dialog: MatDialog, private snackBar: MatSnackBar) {
    this.http = http;
  }

  ngOnInit() {
    // this.timerSubscription = timer(0, 100000).pipe(
    //   map(() => {
        this.refresh(); // load data contains the http request
    //   })
    // ).subscribe();
  }

  refresh() {
    const users$ = this.http.getEmployees();
    users$.subscribe(user => {
      this.employees = user;
    });
  }

  onRowClicked(row) {
    this.selectedRow = row;
    // console.log(this.selectedRow);
  }

  openAddDialog() {
    this.dialog.open(AddDialogComponent);
  }

  openSnackBar(message: string) {
    this.snackBar.open(message, 'Zamknij', {
      duration: 3000
    });
  }

  deleteEmployee(id: number) {
    this.http.deleteEmployee(id).subscribe(res => {
        console.log(res);
        this.openSnackBar('Pracownik o id ' + id + ' poprawnie usunięty');
        this.ngOnInit();
      }, error => {
        this.openSnackBar('Cos poszło nie tak !!!');
      }
    );
  }

  changeActive(id) {
    this.http.changeActive(id).subscribe(x => {
      this.openSnackBar('Zmieniono status pracownika');
      this.ngOnInit();
    }, error => {
      this.openSnackBar('Coś poszło nie tak !!!');
    });
  }
}
