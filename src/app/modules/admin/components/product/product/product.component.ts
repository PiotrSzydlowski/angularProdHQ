import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ProductService} from '../../../../../services/product/product.service';
import {Product} from '../../../../../model/Product';
import {AddProduct} from '../../../../../model/AddProduct ';
import {NgForm} from '@angular/forms';
import {CategoryService} from '../../../../../services/category-service/category.service';
import {SubcategoryService} from '../../../../../services/subcategory-service/subcategory.service';
import {UnitService} from '../../../../../services/unit/unit.service';
import {MatDialog} from "@angular/material/dialog";
import {
  EditContractorDialogComponent
} from "../../contractors/contractors/edit-contractor-dialog/edit-contractor-dialog/edit-contractor-dialog.component";
import {
  EditProductDialogComponentComponent
} from "./editProductDialog/edit-product-dialog-component/edit-product-dialog-component.component";
import {error} from "protractor";


@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  constructor(private httpProduct: ProductService, private httpCategory: CategoryService, private subHttp: SubcategoryService,
              private unitHttp: UnitService, public dialog: MatDialog) {
  }

  products: Product[];
  category: any;
  subcategory: any;
  unit: any;
  model: Partial<AddProduct> = {};
  fileToUpload: File | null = null;
  @ViewChild('formProduct') formProduct!: NgForm;
  @ViewChild('uploadPhotoInput')
  uploadInput: ElementRef;


  reset() {
    this.uploadInput.nativeElement.value = '';
  }

  ngOnInit(): void {
    this.getProduct();
    const unit$ = this.unitHttp.getUnits();
    unit$.subscribe(u => {
      this.unit = u;
    });

    const categories$ = this.httpCategory.getCategoryTree();
    categories$.subscribe(cat => {
      this.category = cat;
    });
  }

  getProduct() {
    this.httpProduct.getProductsList().subscribe({
      next: prod => {
        console.log('Product pobrany');
        this.products = prod;
        console.log('Produkty z metody ' + this.products);
      },
      error: () => {
        console.log('product nie pobrany');
      }
    });
  }

  resizePhoto(photo: string) {
    console.log('kkkkk');
  }

  changeStatus(id: string) {
    this.httpProduct.changeProductStatus(id)
      .subscribe({
        next: () => {
          this.getProduct();
        }
      });
  }

  addProduct(formProduct: NgForm) {
    this.formProduct = formProduct;
    this.httpProduct.addProduct(this.model).subscribe({
      next: () => {
        this.getProduct();
      },
      error: () => {
        console.log('nie pykło - zapis produktu');
      }
    });
    this.formProduct.resetForm();
  }


  changeCategory(value: any) {
    const subcategories$ = this.subHttp.getSubcategoryByParentId(value);
    subcategories$.subscribe(sub => {
      this.subcategory = sub;
    });
  }

  onFileSelected(event) {
    this.fileToUpload = event.target.files[0];
    console.log('ZDJECIE ' + this.fileToUpload.name);
  }

  addPhoto(id: string) {
    this.httpProduct.addPhoto(this.fileToUpload, id)
      .subscribe();
    console.log('ZDJECIE ' + this.fileToUpload.name);
    this.reset();
    this.fileToUpload = null;
    this.uploadInput = null;
  }

  deleteProduct(id: string) {
    this.httpProduct.deleteProduct(id)
      .subscribe({
        next: () => {
          console.log('Produkt usunięty');
          this.getProduct();
        }
      });
  }

  editProduct(product: Product) {
    this.httpProduct.setProduct(product);
    this.dialog.open(EditProductDialogComponentComponent, {
      height: '75%',
      width: '30%'
    });
  }

  refresh() {
    this.getProduct();
  }

  deletePhoto(id: string) {
    this.httpProduct.deletePhoto(id)
      .subscribe({
        next: () => {
          this.getProduct();
        }
      });
  }
}
