import {Component, OnInit, ViewChild} from '@angular/core';
import {ContractorSave} from '../../../../../../../model/ContractorSave';
import {Product} from '../../../../../../../model/Product';
import {ProductService} from "../../../../../../../services/product/product.service";
import {NgForm} from "@angular/forms";
import {CategoryService} from '../../../../../../../services/category-service/category.service';
import {SubcategoryService} from "../../../../../../../services/subcategory-service/subcategory.service";
import {UnitService} from "../../../../../../../services/unit/unit.service";
import {EditProduct} from "../../../../../../../model/EditProduct";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-edit-product-dialog-component',
  templateUrl: './edit-product-dialog-component.component.html',
  styleUrls: ['./edit-product-dialog-component.component.css']
})
export class EditProductDialogComponentComponent implements OnInit {

  model: Partial<Product> = {};
  categoryModel: any;
  subcategoryModel: any;
  editModel: Partial<EditProduct> = {};
  unitModel: any;
  @ViewChild('formEditProduct') formEditProduct!: NgForm;

  constructor(private data: ProductService, private httpCategory: CategoryService, private subHttp: SubcategoryService,
              private unitHttp: UnitService, private snackBar: MatSnackBar) {
  }

  ngOnInit(): void {
    this.setModel();
    const unit$ = this.unitHttp.getUnits();
    unit$.subscribe(u => {
      this.unitModel = u;
    });
    const categories$ = this.httpCategory.getCategoryTree();
    categories$.subscribe(cat => {
      this.categoryModel = cat;
    });
  }

  openSnackBar(message: string) {
    this.snackBar.open(message, 'Zamknij', {
      duration: 3000
    });
  }

  setModel() {
    this.model = this.data.getProduct();
  }

  editProduct(formEditProduct: NgForm) {
    this.formEditProduct = formEditProduct;
    console.log(this.formEditProduct.value);
    this.data.editProduct(this.formEditProduct.value).subscribe(res => {
      this.openSnackBar('Produkt poprawnie zedytowany');
    }, error => {
      this.openSnackBar('coś poszło nie tak');
      console.log('edit product from error  ' + error.error);
    });
  }

  changeCategory(value: any) {
    const subcategories$ = this.subHttp.getSubcategoryByParentId(value);
    subcategories$.subscribe(sub => {
      this.subcategoryModel = sub;
    });
  }
}
