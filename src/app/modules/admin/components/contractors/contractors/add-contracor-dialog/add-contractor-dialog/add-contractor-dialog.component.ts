import {Component, OnInit} from '@angular/core';
import {ContractorSave} from "../../../../../../../model/ContractorSave";
import {NgForm} from "@angular/forms";
import {EmployeeServiceService} from "../../../../../../../services/employeeService/employee-service.service";
import {ContractorServiceService} from "../../../../../../../services/contractor-service/contractor-service.service";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-add-contractor-dialog',
  templateUrl: './add-contractor-dialog.component.html',
  styleUrls: ['./add-contractor-dialog.component.css']
})
export class AddContractorDialogComponent {

  model: Partial<ContractorSave> = {};

  constructor(private http: ContractorServiceService, private snackBar: MatSnackBar) {
  }

  openSnackBar(message: string) {
    this.snackBar.open(message, 'Zamknij', {
      duration: 3000
    });
  }

  saveContractor(form: NgForm) {
    this.http.saveContractor(this.model).subscribe(res => {
        console.log('save contractor from ' + res);
        this.openSnackBar('Kontrahent poprawnie dodany');
        this.model = {};
      }, error => {
        console.log('save contractor from error  ' + error.error);
        this.openSnackBar('Problem z dodaniem Kontrahenta');
      }
    );
  }
}
