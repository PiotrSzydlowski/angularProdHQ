import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ContractorServiceService} from '../../../../../services/contractor-service/contractor-service.service';
import {ContractorGet} from '../../../../../model/ContractorGet';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {
  AddContractorDialogComponent
} from './add-contracor-dialog/add-contractor-dialog/add-contractor-dialog.component';
import {
  EditContractorDialogComponent
} from './edit-contractor-dialog/edit-contractor-dialog/edit-contractor-dialog.component';
import {DataUserServiceService} from '../../../../../services/data-user-service/data-user-service.service';

@Component({
  selector: 'app-contractors',
  templateUrl: './contractors.component.html',
  styleUrls: ['./contractors.component.css']
})
export class ContractorsComponent implements OnInit {

  constructor(http: ContractorServiceService, public dialog: MatDialog, private snackBar: MatSnackBar,
              private dataService: DataUserServiceService) {
    this.http = http;
  }

  private http: ContractorServiceService;
  contractors: ContractorGet[];
  contractorsFilter: ContractorGet[];
  file: File = null;
  searchTerm = '';

  @ViewChild('uploadContractorInput')
  uploadInput: ElementRef;

  ngOnInit() {
    // this.timerSubscription = timer(0, 100000).pipe(
    //   map(() => {
    this.refresh(); // load data contains the http request
    //   })
    // ).subscribe();
  }

  // reset file po uploadzie
  reset() {
    this.uploadInput.nativeElement.value = '';
  }

  refresh() {
    const contractors$ = this.http.getContractors();
    contractors$.subscribe(contractor => {
      this.contractors = contractor;
      console.log('AAAAAA ' + this.contractors);
    }, error => {
      console.log(error.errors);
    });
  }

  openSnackBar(message: string) {
    this.snackBar.open(message, 'Zamknij', {
      duration: 3000
    });
  }

  addContractor() {
    this.dialog.open(AddContractorDialogComponent, {
      height: '75%',
      width: '30%'
    });
  }

  editContractorDialog() {
    this.dialog.open(EditContractorDialogComponent, {
      height: '75%',
      width: '30%'
    });
  }

  allFilter() {
    this.refresh();
  }

  supliersFilter() {
    const suppliers$ = this.http.getSuppliers();
    suppliers$.subscribe(suppliers => {
      this.contractors = suppliers;
    }, error => {
      console.log(error.errors);
    });
  }

  customerFilter() {
    const customers$ = this.http.getCustomers();
    customers$.subscribe(customers => {
      this.contractors = customers;
    }, error => {
      console.log(error.errors);
    });
  }

  deleteContractor(id: string) {
    this.http.deleteContractor(id).subscribe(res => {
        console.log(res);
        this.openSnackBar('Kontrahent o id ' + id + ' poprawnie usunięty');
        this.ngOnInit();
      }, error => {
        this.openSnackBar('Cos poszło nie tak !!!');
      }
    );
  }

  editContractor(contractor: ContractorGet) {
    this.dataService.setContractor(contractor);
    this.editContractorDialog();
  }

  onChange(event) {
    this.file = event.target.files[0];
  }

  onUpload() {
    this.http.uploadFile(this.file).subscribe(res => {
      this.openSnackBar('Dane poprawnie zapisane');
      this.ngOnInit();
      this.reset();
    }, error => {
      if (error.status === 417) {
        this.openSnackBar('Pracownik o takiej nazwie juz istnieje');
        this.reset();
      } else {
        this.openSnackBar('Coś poszło nie tak');
        this.reset();
      }
    });
  }

  search(value: string): void {
    this.contractorsFilter = this.contractors.filter((val) =>
      val.address.toLowerCase().includes(value) || val.name.toLowerCase().includes(value) || val.phone.toLowerCase().includes(value)
    );
  }
}
