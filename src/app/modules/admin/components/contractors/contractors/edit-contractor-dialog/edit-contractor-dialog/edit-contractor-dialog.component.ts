import {Component, OnInit} from '@angular/core';
import {NgForm} from "@angular/forms";
import {ContractorSave} from "../../../../../../../model/ContractorSave";
import {DataUserServiceService} from "../../../../../../../services/data-user-service/data-user-service.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {ContractorServiceService} from "../../../../../../../services/contractor-service/contractor-service.service";

@Component({
  selector: 'app-edit-contractor-dialog',
  templateUrl: './edit-contractor-dialog.component.html',
  styleUrls: ['./edit-contractor-dialog.component.css']
})
export class EditContractorDialogComponent implements OnInit {

  model: Partial<ContractorSave> = {};

  constructor(private dataService: DataUserServiceService, private snackBar: MatSnackBar, private http: ContractorServiceService) {
  }

  openSnackBar(message: string) {
    this.snackBar.open(message, 'Zamknij', {
      duration: 3000
    });
  }

  ngOnInit(): void {
    this.getAndSetModel();
  }

  getAndSetModel() {
    this.model = this.dataService.getContractor();
  }

  editContractor(form: NgForm) {
    this.http.updateContractor(this.model).subscribe(res => {
        console.log('save contractor from ' + res);
        this.openSnackBar('Kontrahent poprawnie zedytowany');
        this.model = {};
      }, error => {
        console.log('save contractor from error  ' + error.error);
        this.openSnackBar('Problem z edycją Kontrahenta');
      }
    );
  }
}
