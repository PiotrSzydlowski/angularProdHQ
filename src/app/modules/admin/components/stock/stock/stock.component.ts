import { Component, OnInit } from '@angular/core';
import {SetpromoComponent} from "./setpromo/setpromo.component";
import {Store} from "../../../../../model/Store";
import {StoreService} from "../../../../../services/store-service/store.service";
import {MatDialog} from "@angular/material/dialog";
import {MatSnackBar} from "@angular/material/snack-bar";
import {StockService} from "../../../../../services/stock/stock.service";
import {StockItem} from "../../../../../model/StockItem ";
import {AddQtyComponent} from "./add-qty/add-qty.component";
import {MinusQtyComponent} from "./minus-qty/minus-qty.component";
import {ChangePriceComponent} from "./change-price/change-price.component";
import {SetAtribiuteComponent} from "./set-atribiute/set-atribiute.component";

@Component({
  selector: 'app-stock',
  templateUrl: './stock.component.html',
  styleUrls: ['./stock.component.css']
})
export class StockComponent implements OnInit {

  store: Store[];
  stock: StockItem[];
  mag: any;

  constructor(private httpStore: StoreService, private httpStock: StockService, public dialog: MatDialog, private snackBar: MatSnackBar) {
  }

  ngOnInit(): void {
    this.getStore();
  }

  getStore() {
    this.httpStore.getStoresList().subscribe({
      next: store => {
        this.store = store;
      }
    });
  }

  changeMag(value) {
    this.mag = value;
    console.log('VALUE MAG ' + value);
    this.getStockByMagId(value);
  }

  getStockByMagId(id: string) {
    this.httpStock.getStockListByMagId(id).subscribe({
      next: stock => {
        this.stock = stock;
      }
    });
  }

  addToStock(id: number) {
    this.httpStock.setDataToAddQty(this.mag, id);
    this.dialog.open(AddQtyComponent, {
      height: '20%',
      width: '20%'
    });
    this.dialog.afterAllClosed.subscribe(() => {
      this.getStockByMagId(this.mag);
    });
  }

  minusFromStock(id: number) {
    this.httpStock.setDataToAddQty(this.mag, id);
    this.dialog.open(MinusQtyComponent, {
      height: '20%',
      width: '20%'
    });
    this.dialog.afterAllClosed.subscribe(() => {
      this.getStockByMagId(this.mag);
    });
  }

  changePrice(productId) {
    this.httpStock.setDataToAddQty(this.mag, productId);
    this.dialog.open(ChangePriceComponent, {
      height: '20%',
      width: '20%'
    });
    this.dialog.afterAllClosed.subscribe(() => {
      this.getStockByMagId(this.mag);
    });
  }

  openSnackBar(message: string) {
    this.snackBar.open(message, 'Zamknij', {
      duration: 3000
    });
  }

  setPromo(productId: number, promo: boolean) {
    this.httpStock.setDataToAddQty(this.mag, productId);
    if (promo === true) {
      this.httpStock.setPromo(this.mag, productId, 1.00)
        .subscribe({
          next: () => {
            this.openSnackBar('Poprawnie usunięto promocję');
            this.getStockByMagId(this.mag);
          }, error: () => {
            this.openSnackBar('Coś poszło nie tak');
          }
        });
    } else {
      this.dialog.open(SetpromoComponent, {
        height: '20%',
        width: '20%'
      });
    }
    this.dialog.afterAllClosed.subscribe(() => {
      this.getStockByMagId(this.mag);
    });
  }

  setAtribute(productId: number, hit: boolean, cold: boolean, _new: boolean) {
    this.httpStock.setArtibiute(cold, hit, _new);
    this.httpStock.setDataToAddQty(this.mag, productId);
    this.dialog.open(SetAtribiuteComponent, {
      height: '30%',
      width: '15%'
    });
    this.dialog.afterAllClosed.subscribe(() => {
      this.getStockByMagId(this.mag);
    });
  }
}
