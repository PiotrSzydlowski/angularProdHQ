import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SetAtribiuteComponent } from './set-atribiute.component';

describe('SetAtribiuteComponent', () => {
  let component: SetAtribiuteComponent;
  let fixture: ComponentFixture<SetAtribiuteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SetAtribiuteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SetAtribiuteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
