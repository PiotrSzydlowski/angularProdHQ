import {Component, OnInit} from '@angular/core';
import {StockService} from "../../../../../../services/stock/stock.service";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-set-atribiute',
  templateUrl: './set-atribiute.component.html',
  styleUrls: ['./set-atribiute.component.css']
})
export class SetAtribiuteComponent {

  cold: boolean;
  new: boolean;
  hit: boolean;

  constructor(private http: StockService, private snackBar: MatSnackBar) {
  }

  openSnackBar(message: string) {
    this.snackBar.open(message, 'Zamknij', {
      duration: 3000
    });
  }

  changeAtribute() {
    this.http.setAttributes(this.cold, this.hit, this.new, this.http.geMagData() ,this.http.getProductId())
      .subscribe({
        next: () => {
          this.openSnackBar('Poprawnie zmieniono atrybuty');
        }, error: () => {
          this.openSnackBar('Coś poszło nie tak');
        }
      });
  }

  changeNew(value: any) {
    this.new = value;
  }

  changeCold(value: any) {
    this.cold = value;
  }

  changeHit(value: any) {
    this.hit = value;
  }
}
