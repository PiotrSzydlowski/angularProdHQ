import { Component, OnInit } from '@angular/core';
import {StockService} from "../../../../../../services/stock/stock.service";

@Component({
  selector: 'app-add-qty',
  templateUrl: './add-qty.component.html',
  styleUrls: ['./add-qty.component.css']
})
export class AddQtyComponent implements OnInit {

  constructor(private httpStock: StockService) { }

  ngOnInit(): void {
  }

  onSubmit(event: any) {
    const mag = this.httpStock.geMagData();
    const productId = this.httpStock.getProductId();
    this.httpStock.addQtyToStock(mag, productId, event.target.qty.value)
      .subscribe();
  }


}
