import { Component, OnInit } from '@angular/core';
import {MatSnackBar} from "@angular/material/snack-bar";
import {StockService} from "../../../../../../services/stock/stock.service";

@Component({
  selector: 'app-setpromo',
  templateUrl: './setpromo.component.html',
  styleUrls: ['./setpromo.component.css']
})
export class SetpromoComponent {

  constructor(private httpStock: StockService, private snackBar: MatSnackBar) {
  }

  openSnackBar(message: string) {
    this.snackBar.open(message, 'Zamknij', {
      duration: 3000
    });
  }

  onSubmit(event: any) {
    this.httpStock.setPromo(this.httpStock.geMagData(), this.httpStock.getProductId(), event.target.price.value)
      .subscribe({
        next: () => {
          this.openSnackBar('Poprawnie ustawiono promocję');
        }, error: () => {
          this.openSnackBar('Coś poszło nie tak');
        }
      });
  }


}
