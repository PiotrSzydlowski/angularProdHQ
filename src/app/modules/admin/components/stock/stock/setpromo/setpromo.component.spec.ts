import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SetpromoComponent } from './setpromo.component';

describe('SetpromoComponent', () => {
  let component: SetpromoComponent;
  let fixture: ComponentFixture<SetpromoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SetpromoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SetpromoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
