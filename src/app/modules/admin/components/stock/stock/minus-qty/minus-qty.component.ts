import { Component, OnInit } from '@angular/core';
import {StockService} from "../../../../../../services/stock/stock.service";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-minus-qty',
  templateUrl: './minus-qty.component.html',
  styleUrls: ['./minus-qty.component.css']
})
export class MinusQtyComponent {

  constructor(private httpStock: StockService, private snackBar: MatSnackBar) {
  }


  openSnackBar(message: string) {
    this.snackBar.open(message, 'Zamknij', {
      duration: 3000
    });
  }

  onSubmit(event: any) {
    const mag = this.httpStock.geMagData();
    const productId = this.httpStock.getProductId();
    this.httpStock.minusQtyToStock(mag, productId, event.target.qty.value)
      .subscribe({
        next: () => {
          this.openSnackBar('Poprawnie zmniejszono ilość na stanie');
        }, error: () => {
          this.openSnackBar('Nie można zmniejszyć ilości na stanie');
        }
      });
  }
}
