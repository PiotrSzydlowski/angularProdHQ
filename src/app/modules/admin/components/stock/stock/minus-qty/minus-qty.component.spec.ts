import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MinusQtyComponent } from './minus-qty.component';

describe('MinusQtyComponent', () => {
  let component: MinusQtyComponent;
  let fixture: ComponentFixture<MinusQtyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MinusQtyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MinusQtyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
