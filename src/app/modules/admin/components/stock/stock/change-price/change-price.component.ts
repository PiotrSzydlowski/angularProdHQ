import { Component, OnInit } from '@angular/core';
import {StockService} from "../../../../../../services/stock/stock.service";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-change-price',
  templateUrl: './change-price.component.html',
  styleUrls: ['./change-price.component.css']
})
export class ChangePriceComponent  {

  constructor(private httpStock: StockService, private snackBar: MatSnackBar) { }


  onSubmit(event: any) {
    this.httpStock.changeProductPrice(event.target.price.value, this.httpStock.geMagData(), this.httpStock.getProductId())
      .subscribe({
        next: () => {
          this.openSnackBar('Poprawnie zmieniono cenę');
        }, error: () => {
          this.openSnackBar('Coś poszło nie tak');
        }
      });
  }

  openSnackBar(message: string) {
    this.snackBar.open(message, 'Zamknij', {
      duration: 3000
    });
  }


}
