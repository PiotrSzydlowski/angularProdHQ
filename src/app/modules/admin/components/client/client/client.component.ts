import { Component, OnInit } from '@angular/core';
import {Customers} from "../../../../../model/Customers";
import {MatDialog} from "@angular/material/dialog";
import {CustomerService} from "../../../../../services/customer/customer.service";
import {UserService} from "../../../../../services/users/user.service";

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css']
})
export class ClientComponent implements OnInit {

  customers: Customers[];

  constructor(private httpCustomer: CustomerService, public dialog: MatDialog, private httpUser: UserService) { }

  ngOnInit(): void {
    this.getCustomers();
  }

  getCustomers() {
    this.httpCustomer.getCustomers().subscribe({
      next: customers => {
        console.log('Klienci pobrany');
        this.customers = customers;
      },
      error: () => {
        console.log('Klienci nie pobrane');
      }
    });
  }

  getaddress(id: string) {

  }

  orders(id: string) {

  }

  banned(id: string) {
    this.httpUser.changeUserBanned(id)
      .subscribe({
        next: () => {
          this.getCustomers();
        }
      });
  }
}
