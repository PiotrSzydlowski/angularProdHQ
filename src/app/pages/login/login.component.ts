import {Component, Injectable, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {LoginService} from '../../services/auth-service/login.service';
import {LoginUserPost} from '../../model/LoginUserPost';
import {Router} from '@angular/router';
import {AuthService} from '../../services/auth-service/auth.service';
import {DataUserServiceService} from '../../services/data-user-service/data-user-service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  model: Partial<LoginUserPost> = {};
  errorFlag = false;
  userLogin: string;

  constructor(private http: LoginService, private router: Router, private auth: AuthService,
              private data: DataUserServiceService) {
  }

  ngOnInit(): void {
    if (this.auth.isLoggedIn()) {
      this.router.navigate(['admin']);
    }
  }


  postLogin(form: NgForm) {
    this.http.logIn(this.model).subscribe(
      result => {
        if (result.body.login !== this.model.login) {
          this.router.navigate(['/admin']);
          this.auth.setToken();
          this.data.setData(result.body.login);
        }
      },
      error => {
        this.errorFlag = true;
      }
    );
    form.resetForm();
    this.errorFlag = false;
  }


}
